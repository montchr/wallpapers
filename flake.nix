{
  description = "Seadome Wallpapers";
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.flake-parts.url = "github:hercules-ci/flake-parts";
  outputs =
    inputs@{ nixpkgs, flake-parts, ... }:
    let
      inherit (nixpkgs) lib;
    in
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = lib.systems.flakeExposed;
      perSystem =
        { pkgs, config, ... }:
        {
          packages = {
            default = config.packages.seadome-wallpapers;
            seadome-wallpapers = pkgs.stdenv.mkDerivation {
              pname = "seadome-wallpapers";
              version = "unstable-2024-04-05";
              src =
                with lib.fileset;
                let
                  hasImageExt =
                    file:
                    builtins.any file.hasExt [
                      "jpg"
                      "jpeg"
                      "png"
                    ];
                  fileset = (fileFilter hasImageExt ./src);
                in

                toSource {
                  root = ./src;
                  inherit fileset;
                };
              installPhase = ''
                runHook preInstall

                mkdir -p $out/share/wallpapers/
                cp * $out/share/wallpapers/

                runHook postInstall
              '';
              meta = with lib; {
                description = "Wallpapers collected for Seadome Systems";
                platforms = platforms.all;
                maintainers = with maintainers; [ montchr ];
              };
            };
          };
        };
    };
}
